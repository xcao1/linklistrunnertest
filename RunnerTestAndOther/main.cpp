#include<iostream>
#include "MyLinkList.h"

using namespace std;

int main(){
	NodeDouble<int> *head = new NodeDouble<int>;
	head->mData = 0;

	NodeDouble<int> *ptr = head;

	for(int i = 1; i != 11; i++){
		ptr->next = new NodeDouble<int>;
		ptr->next->prev = ptr;
		ptr->next->mData = i;

		ptr = ptr->next;
	}
	ptr->next = head;

	NodeDouble<int> *encounter = nullptr;

	if(DoubleLinkedList<int>::ExistRing(head, encounter)){
		cout<<"exist ring";
		cout<<"\nring length :";
		cout<<DoubleLinkedList<int>::RingLength(encounter);
	}else{
		NodeDouble<int> *ptrPrint = head;
		while(ptrPrint){
			cout<<ptrPrint->mData;
			ptrPrint = ptrPrint->next;
		}
	}


	system("pause");
	return 0;
}