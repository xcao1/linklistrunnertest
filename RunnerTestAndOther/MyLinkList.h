#ifndef __MY_LINK_LIST__
#define __MY_LINK_LIST__
// =============================================

// ================================================
// single linked
template<class DataType>
struct Node{
	DataType mData;
	struct Node *next;
	Node():mData(),next(nullptr){}
	/*typedef Node* NodePtr;*/
};
//
//template<class DataType>
//using NodePtr = Node<DataType> *;

// ================================================
// double linked
template<class DataType>
struct NodeDouble{
	DataType mData;
	struct NodeDouble *prev;
	struct NodeDouble *next;
	NodeDouble():mData(),prev(nullptr),next(nullptr){}
	/*typedef NodeDouble* NodeDoublePtr;*/
};/*
template<class DataType>
using NodeDoublePtr = NodeDouble<DataType>*;*/


template<class DataType>
class DoubleLinkedList{
	/*NodeDouble<DataType> *mHeadNodePtr;*/

public:
	/*DoubleLinkedList():mHeadNodePtr(nullptr){}*/

	// check if there are rings in the double linked list
	// also return the encounter node
	static bool ExistRing(NodeDouble<DataType> *entry, NodeDouble<DataType> *&encounter){
		if(entry){
			NodeDouble<DataType> *slow = entry;// slow runner
			NodeDouble<DataType> *quick = entry;// quick runner

			while(slow->next&&quick->next&&quick->next->next){
				slow = slow->next;
				quick = quick->next->next;
				if(slow == quick){
					encounter = quick;
					return true;
				}
			}
			return false;
		}else{
			cerr<<"error : entyr node is empty.";
			throw("entry node is not initialized.");
			return false;
		}
	}

	// recieve a encounter node, 
	// which is the position that slow runner meet with quick runner
	// return the length
	static int RingLength(NodeDouble<DataType> *encounter){
		if(encounter){
			int length = 0;
			NodeDouble<DataType> *runner = encounter;// runner
			while(runner){
				runner = runner->next;
				length++;
				if(runner == encounter)
					break;
			}
			if(!runner){
				cerr<<"error : the runner become nullptr";
				throw("error : the runner become nullptr");
				return -1;
			}else
				return length;
		}else{
			cerr<<"error : encounter node is empty.";
			throw("encounter node is not initialized.");
			return -1;
		}
	}
};



// =============================================
#endif // !__MY_LINK_LIST__
